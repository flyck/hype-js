import ReactPlayer from 'react-player';

export const ReactExample = function ReactExample() {

  return (
    <ReactPlayer url='https://www.youtube.com/watch?v=ysz5S6PUM-U' />
  );
};

export default ReactExample;
