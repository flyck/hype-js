// https://bun.sh/docs/runtime/typescript#add-dom-types
import { Context, S3Event } from "aws-lambda"
import { S3Client, GetObjectCommand } from '@aws-sdk/client-s3';
import * as Jimp from 'jimp'
import { Readable } from "stream";
import { writeFile } from 'fs';
import { promisify } from 'util'

// aws subobject is special bun stuff
async function handler(request: { aws: S3Event }) {
  console.debug("event: " + JSON.stringify(request))
  const record = request.aws.Records[0]

  const client = new S3Client({
    region: "eu-central-1"
  })
  const getCommand = new GetObjectCommand({
    Bucket: record.s3.bucket.name,
    Key: record.s3.object.key
  })

  // no worky :(
  const image1 = await Jimp.read("./original.png");
  const writtenImage = await image1.writeAsync("./test.png");

  const getResponse = await client.send(getCommand)
  // Access the object's data as a Buffer
  const bufferData: Buffer = await streamToBuffer(getResponse.Body as Readable)
  const img = await Jimp.read(bufferData)
  await new Promise<void>((res, rej) => {
    writeFile("original.png", bufferData, (err) => {
      if (err) rej()
      res()
    })
  })

  const rotateAsync = promisify(img.rotate)
  await rotateAsync(90)
  console.log("Done")
}

function streamToBuffer(stream: Readable): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    const chunks: Buffer[] = [];
    stream.on("data", (chunk: Buffer) => chunks.push(chunk));
    stream.on("end", () => resolve(Buffer.concat(chunks)));
    stream.on("error", reject);
  });
}

export default { handler }
export { handler }
