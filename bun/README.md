# bun-lambda

https://bun.sh/docs/quickstart

To install dependencies:

```bash
bun install
```

To run:

```bash
bun run src/index.ts
```

This project was created using `bun init` in bun v0.6.14. [Bun](https://bun.sh) is a fast all-in-one JavaScript runtime.

## Serverless

Issues related to running it offline:
- https://github.com/dherault/serverless-offline/issues/1152 (provided.al2 layout zip download broken)
